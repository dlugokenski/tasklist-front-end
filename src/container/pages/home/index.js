import React from 'react';
import { Layout as LayoutAntd, Input, Row, Form, Col, Button } from 'antd';
import styled from 'styled-components';

import { cadastrarAtividade, buscarListaTarefas } from 'Provider/TaskList';
import Lista from 'Componente/Lista';

const {  Content } = LayoutAntd;
const FormItem = Form.Item;
const { TextArea } = Input;

const ImputStyle = styled(Input)`
  max-width: 200px;
`;


const Layout = styled(LayoutAntd)`
  background: #e6deca;
  position: absolute;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;


class Main extends React.Component {


  state = {
    data: [],
    modoEditar: false
  }

  componentDidMount(){
    this.buscarDataTaskList();
  }

  async buscarDataTaskList(){
    const data = await buscarListaTarefas();
    this.setState({loading : false, data})  
  }

  cadastrarIten(){
    this.setState({loading : true})
    const { data = [] } = this.state;
    const { validateFields } = this.props.form;

    validateFields(async (err, values) => {
      if (!err) {
        const title = values.title;
        const descricao = values.descricao;
        const statusConcluido =  false;
        const json = await cadastrarAtividade({  objeto: {title, descricao, status: statusConcluido} });
        data.push(json);
      }
    });
    this.setState({loading : false, data})    
  }

  concluidoAtividade = (item) => { 
    const { data = [] } = this.state;
    const indexItem = data.indexOf(item);
    data[indexItem].statusConcluido = !data[indexItem].statusConcluido;
    this.setState({data});
  }

  excluirLista = (item) => {
    const { data = [] } = this.state;
    const indexItem = data.indexOf(item);
    data.splice(indexItem,1);
    this.setState({data});
  }

  editar = (item) => {
    const { form: { setFieldsValue } } = this.props;

    setFieldsValue({
      title:  item.title,
      descricao: item.descricao
  });
  
  this.setState({ modoEditar: true });
  }

  montarButtons(){
    const { modoEditar = false } = this.state;

    return (
      modoEditar ? (
        <Row gutter={16}>
          <div style={{float: 'right'}}>
            <Button 
              type="primary" 
              onClick={() => this.cadastrarIten()} 
            >
            {'Editar'}
            </Button>
            <Button 
              type="danger" 
              onClick={() => this.setState({modoEditar: false})} 
              style={{ marginLeft: '10px'}}
            >
            {'Cancelar'}
            </Button>
          </div>
        </Row>
      ) : (
        <div style={{float: 'right'}}>
          <Button 
            type="primary" 
            onClick={() => this.cadastrarIten()} 
          >
          {'Adicionar'}
          </Button>
        </div>
      )
    )
  }

  salvarLista(){

  }

   render() {  
    const { data = [], loading = false } = this.state;
    const { getFieldDecorator } = this.props.form;

    return (
      <Layout >
        <Content>
          <div  style = {{  maxWidth: '600px',}}>
            <Row >
            <Form
                style={{
                    backgroundColor: 'transparent',
                    borderRadius: 6,
                    padding: 12,
                }}
                >
                  <div style={{ textAlign: 'center',  fontSize: '40px' }}>
                    Tasklist
                  </div>
                  <Row gutter={16} style-={{}} >
                      <FormItem label="Titulo">
                        {getFieldDecorator('title', {
                        rules: [{ required: false }],
                        })(  <Input />)}
                      </FormItem>
                      <FormItem label="Descrição">
                        {getFieldDecorator('descricao', {
                          rules: [{ required: false }],
                          })( <TextArea  rows="5"/>)}
                    
                      </FormItem>
                    <FormItem>
                          {this.montarButtons()}
                </FormItem>
                  </Row>
                </Form>
            </Row>
            <Row>
              <Lista 
                concluidoAtividade={this.concluidoAtividade} 
                excluirLista = { this.excluirLista}
                editar = { this.editar }
                data = { data } 
              />
            </Row>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default Form.create()(Main);


  