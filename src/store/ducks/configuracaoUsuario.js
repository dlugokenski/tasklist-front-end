import { createActions, createReducer } from 'reduxsauce'

const INITIAL_STATE = { permissao: 'nao'};

export const { Types, Creators } = createActions({
    adicionarConfiguracao: ["json"]
})

const setConfiguracao = ( state = INITIAL_STATE, action ) =>  ({...state, ...action.json});

  
export default createReducer(INITIAL_STATE, {
    [Types.ADICIONAR_CONFIGURACAO]: setConfiguracao
})
