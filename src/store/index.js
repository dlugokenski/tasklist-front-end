import { applyMiddleware, createStore, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';

import history from '../router/history';
import reducers from './ducks';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers(history), // root reducer with router state
  composeEnhancer(
    applyMiddleware(
      routerMiddleware(history), // for dispatching history actions
    ),
  ),
);

export default store;
