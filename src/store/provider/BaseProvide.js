const urlBase = `http://localhost:8081/rest/`
// const urlBase = `/rest/`

const chegarErro = (response) => {
    if (response && response.mensagem === 'sessao_expirada') {
      sessionStorage.removeItem('tokenHash');
      // <Redirect to='/login'/>
        return true;
    }
    return false
}

const GET = async (url) => {
  const urlHttp = `${urlBase}${url}`
  try {
    const response = await fetch(urlHttp).then(resposta => resposta.json().then( data => data));
    try {
      if(!chegarErro(response)){
          return response;
      }  
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada GET: ${urlHttp}`);
  }
};

const POST = async (url, body) => {
  const urlHttp = `${urlBase}${url}`
  try {
    const response = await fetch(urlHttp, {
      method: 'POST',
      body: JSON.stringify(body),
    }).then(resposta => resposta.json().then( data => data));
    try {
      if(!chegarErro(response)){
          return response;
      }  
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada POST: ${urlHttp}`);
  }
};

const DELETE = async (url, body) => {
  const urlHttp = `${urlBase}${url}`
  try {
    const response = await fetch(urlHttp, {
      method: 'DELETE',
      body: JSON.stringify(body),
    });
    try {
      if(!chegarErro(response)){
          return response;
      }  
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada POST: ${urlHttp}`);
  }
};

const UPLOAD = async (url, body) => {
  const urlHttp = `${urlBase}${url}`
  try {
    const response = await fetch(urlHttp, {
      method: 'POST',
      body,
    }).then(resposta => resposta.json().then( data => data));
    try {
      if(!chegarErro(response)){
          return response;
      }  
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada UPLOAD: ${url}`);
  }
};

export default {
  GET,
  POST,
  DELETE,
  UPLOAD
};
