import BaseProvider from './BaseProvide'

export const cadastrarAtividade = (parans) => {
    return BaseProvider.POST('taskList/cadastrar', parans);
}

export const buscarListaTarefas = () => {
    return BaseProvider.GET(`taskList/consultar`);
};
 