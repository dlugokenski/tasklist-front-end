import React from 'react'
import { List, Card, Tooltip, Button  } from 'antd';

const {  Item } = List;
const { Meta } = Item;
export default (props) => {
    const { data = [] } = props;
    return(
        <Card title="Lista de tarefas">
            <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={data}
                renderItem={item => (
                <Item
                    actions={[
                        <Tooltip placement="left" title="Concluir tarefa">
                            <Button 
                                disabled = {item.statusConcluido}
                                style={{ float: "right", position: 'relative'}}
                                type="primary" 
                                icon="check"
                                shape="circle" 
                                onClick={() => props.concluidoAtividade(item)}
                            />
                        </Tooltip>,
                        <Tooltip placement="left" title="Editar tarefa">
                            <Button 
                                style={{ float: "right", position: 'relative'}}
                                disabled = {item.statusConcluido}
                                type="primary" 
                                icon="edit"
                                shape="circle" 
                                onClick={() => props.editar(item)}
                            />
                        </Tooltip>,
                        <Tooltip placement="left" title="Excluir lista">
                            <Button 
                                style={{ float: "right", position: 'relative'}}
                                type="danger" 
                                icon="delete"
                                shape="circle" 
                                onClick={() => props.excluirLista(item)}
                            />
                        </Tooltip>,
                 ]}
                >
                    <Meta
                        style={{ textDecoration: item.statusConcluido ? 'line-through' : 'undefined' }}
                        title={item.title || "" }
                        description={item.descricao || ""}
                    />
                </Item>
                )}
            />
        </Card>
    )
}