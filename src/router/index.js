import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import Home from 'Pages/home'
import { PrivateRoute } from 'RoutersPage/PrivateRoute'


export default () =>(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={Home} />
      <PrivateRoute path='/*' component={Home}/>
    </Switch>
  </BrowserRouter>
)

