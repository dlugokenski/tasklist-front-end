const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const path = require('path');

module.exports = function override(config, env) {
  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
    config,
  );

  config.resolve = {
    alias: {
      Componente: path.resolve(__dirname, 'src/componente/'),
      Pages: path.resolve(__dirname, 'src/container/pages/'),
      Provider: path.resolve(__dirname, 'src/store/provider/'),
      Ducks: path.resolve(__dirname, 'src/store/ducks/'),
      RoutersPage: path.resolve(__dirname, 'src/router/'),
      Util: path.resolve(__dirname, 'src/util/'),
    }
  };

  config = rewireLess.withLoaderOptions({
    modifyVars: { },
    javascriptEnabled: true,
  })(config, env);
  return config;
};
